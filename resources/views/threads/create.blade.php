@extends('layouts.app')

@section('title', 'Create New Thread')

@section('content')
@include('threads.partials.landing')
<div class="container my-5">
    <div class="col-md-12 row header-font my-3">
        Create New Thread
    </div>
    <div class="row">
        <div class="col-md-4">
            @include('threads.partials.sidebar')
        </div>
        <div class="col-md-8">
            @include('alert')
            <div class="card thread">
                <div class="card-body">
                    <form action="{{ route('threads.create') }}" method="post" autocomplete="off">
                        @csrf
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" name="title" id="title" class="form-control" required>
                        </div>

                        <div class="form-group">
                            <label for="subject">Subject</label>
                            <select name="subject" id="subject" class="form-control" required>
                                <option disabled selected>Choose subject</option>
                                @foreach ($subjects as $subject)
                                    <option value="{{ $subject->id }}">{{ $subject->name }}</option>    
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea name="description" id="description" class="form-control" rows="10" required></textarea>
                        </div>

                        <button type="submit" class="btn btn-success">SUBMIT</button>
                    </form>


                </div>
            </div>
        </div>
    </div>
</div>
@include('threads.partials.footer')
@endsection
