@extends('layouts.app')

@section('content')
<section class="home pt-5">
    <div class="row hero-index">
        <div class="hero-logo1 col-md-6 mt-5">
            <img src="{{ asset('img/gglogo.png') }}" class="logo">
            <img src="{{ asset('img/A Place to Discussabout Games.png') }}" class="slogan">
        </div>
        <div class="hero-logo2 col-md-6 mt-5">
            <img src="{{ asset('img/Group 14.png') }}" class="logo2">
        </div>
        <div class="hero-logo3 col-md-12">
            <a href="#about">
                <img src="{{ asset('img/scrolldown.png') }}">
            </a>
        </div>
    </div>
    <div class="row about pt-5" id="about">
        <div class="about-gambar col-md-6 pl-5">
            <div class="about-gambar-top">
                <img src="{{ asset('img\android.png') }}" class="gambar-top-kiri">
                <img src="{{ asset('img\virtual.png') }}" class="gambar-top-kanan">
            </div>
            <div class="about-gambar-bottom">
                <img src="{{ asset('img\stick.png') }}" class="gambar-bottom">
            </div>
        </div>
        <div class="d-flex align-items-center justify-content-end col-md-6 pr-5">
            <div class="d-flex flex-column about-container">
                <span class="about-heading">About Us</span>
                <div class="border">
                </div>
                <p class="about-text">
                    Game Global is a place for people around the world to talk about games from various platforms,
                    gaming hardware, gaming news, and many more. The site has been up since 2020 by gamers who'd like to
                    have a platform where they can discuss about their interests, opinions, and reviews.<br><br>

                    We are welcome for you gamers who wants to join the community by signing in to the website and share
                    some of your opinions about games from pc, console, mobile, or any other platform from any genre.
                </p>
                <img src="{{ asset('img/gglogo.png') }}" class="logo-about">
            </div>
        </div>
    </div>
    <div class=" d-flex flex-row choice mt-3 pb-5 row">
        <span class="choice-judul col-md-12 pl-5 mt-3">Game Discussion Topics</span>
        <div class="choice-border col-md-4 ml-5">
        </div>
        <div class="d-flex col-md-12 justify-content-around mt-3 cardheader">
            <div class="card" style="width: 18rem;">
                <img class="card-img-top" src="{{ asset('img/ps5.png') }}" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title">PlayStation 5 Launch !</h5>
                    <p class="card-text">How's your opinion on the new PS 5 that is open for pre-order? Is it a fair
                        price? Do you think it will be a more reliable console than the previous generation?</p>
                    <a href="#" class="btn">Learn More</a>
                </div>
            </div>
            <div class="card" style="width: 18rem;">
                <img class="card-img-top" src="{{ asset('img/Y3QiU3C3coiBfbfCMkfDEA.png') }}" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title">Ghost of Tsushima is out !</h5>
                    <p class="card-text">Ghost of Tsushima is an action-adventure stealth game played from a
                        third-person perspective. It features a large open world without any waypoints and can be
                        explored without guidance.</p>
                    <a href="#" class="btn">Learn More</a>
                </div>
            </div>
            <div class="card" style="width: 18rem;">
                <img class="card-img-top" src="{{ asset('img/tlou2.png') }}" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title">TLOU 2: Disappointing ?</h5>
                    <p class="card-text">Its great fun and compelling doest go the way people wanted and goes for a
                        darker less forgiving tone where the characters actions have consequencess.</p>
                    <a href="#" class="btn">Learn More</a>
                </div>
            </div>
            <div class="card" style="width: 18rem;">
                <img class="card-img-top" src="{{ asset('img/RTX-3080.png') }}" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title">RTX 3080 !!!</h5>
                    <p class="card-text">Historically, Nvidia staggers the launch of a new consumer GPU family, and we
                        expect this will be the case again. This means that the flagship and top-tier cards.</p>
                    <a href="#" class="btn">Learn More</a>
                </div>
            </div>
        </div>
    </div>
</section>
@include('threads.partials.footer')
@endsection
