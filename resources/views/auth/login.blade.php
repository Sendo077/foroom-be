@extends('layouts.app')

@section('content')
<div class="container_regist no_padding">
    <div class="background_image"></div>
    <div class=" col-sm-12 col-md-5 background_image_blur_login"></div>

    <div class="login_section text-white d-flex flex-column col-md-4">
        <h1 class="mb-0 text-center">Login</h1>
        <form form method="POST" action="{{ route('login') }}">
            @csrf
            <div class="form-group">
                <label class="label_input" for="username">{{ __('E-Mail Address') }}</label>
                <input id="email" class="short_input form-control @error('email') is-invalid @enderror" name="email"
                    value="{{ old('email') }}" type="email" placeholder="Enter your Email Address" required
                    autocomplete="email" autofocus>
                @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group">
                <label class="label_input" for="password">{{ __('Password') }}</label>
                <input id="password" class="short_input form-control @error('password') is-invalid @enderror"
                    name="password" type="password" placeholder="Enter your Password" required
                    autocomplete="current-password">

                @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>

            <div class="form-check">
                <input class="form-check-input" type="checkbox" name="remember" id="remember"
                    {{ old('remember') ? 'checked' : '' }}>

                <label class="form-check-label" for="remember">
                    {{ __('Remember Me') }}
                </label>
            </div>

            <p>New to Game globe? Register <a href="{{ route('register') }}">Here</a></p>

            <p class="d-flex justify-content-between">
                @if (Route::has('password.request'))
                <a class="btn button_forgot" href="{{ route('password.request') }}">
                    {{ __('Forgot Your Password?') }}
                </a>
                @endif


                <button class="btn button_login" type="submit">Login</button>
            </p>
        </form>

        <span>
            <a href="{{ url()->previous() }}" class="back_link">Back</a>
        </span>
    </div>
</div>
@endsection
