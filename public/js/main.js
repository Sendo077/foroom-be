function confirmPassword() {
    if ($("#pass").val() == $("#confirm-pass").val()) {
        $("#confirm-pass")[0].setCustomValidity("");
        $("#confirmPass-message").text("");
    } else {
        $("#confirm-pass")[0].setCustomValidity("Match your password");
        $("#confirmPass-message").text("Match your password");
    }
}
