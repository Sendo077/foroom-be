<?php

namespace App\Http\Controllers\Forum;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Subject;
use App\Thread;

class SubjectController extends Controller
{
    public function show(Subject $subject) {
        $threads = $subject->threads()->latest()->paginate(5);
        $subjects= Subject::all();
        return view('threads.index', compact('threads', 'subjects'));
    }
}
